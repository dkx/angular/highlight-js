import {NgModule} from '@angular/core';

import {HighlightjsComponent} from './highlightjs.component';


@NgModule({
	declarations: [
		HighlightjsComponent,
	],
	exports: [
		HighlightjsComponent,
	],
})
export class HighlightjsModule {}
