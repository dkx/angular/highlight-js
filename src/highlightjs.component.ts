import {Component, Input, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import * as hljs from 'highlight.js';


@Component({
	selector: 'dkx-highlightjs',
	templateUrl: './highlightjs.component.html',
})
export class HighlightjsComponent implements AfterViewInit
{


	@Input()
	public code: string;

	@Input()
	public language: string = '';

	@ViewChild('codeBlock')
	private codeBlock: ElementRef;


	public ngAfterViewInit(): void
	{
		hljs.highlightBlock(this.codeBlock.nativeElement);
	}

}
